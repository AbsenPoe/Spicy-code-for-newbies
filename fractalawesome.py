


import turtle
import math

def square(size, t, isLeft):

	"""
	Hi, we are going to create a fractal tree. To start, we need to create
	the first square. We are going to need a size and direction. Direction could be 
	"right" or "left".
	
	"""
	
	t2 = t.clone()
	
	t.right(90)
	t.forward (size)
	t2.forward (size)
	t.left(90)
	t.forward (size)
	t2.right(90)
	t2.forward(size)
	
	if (isLeft):
	
		t.left(90)
		t.forward(size)
	
	else:
		
		t2.right(90)
		t2.forward(size)		

	return t2
	
def fractalizing (sizeSquare, dirSquare, minSize, t, isLeft):
	
	if sizeSquare <= minSize:
		return
		
	t2 = square (sizeSquare, t, isLeft)
	t.right (dirSquare)
	t2.left (90 + dirSquare)
	fractalizing (sizeSquare/math.sqrt(2), dirSquare, minSize, t, True)
	fractalizing (sizeSquare/math.sqrt(2), dirSquare, minSize, t2, False)
	
	
if __name__ == "__main__":

	t = turtle.Pen()
	t.setheading (90)
	t.hideturtle()
	t.speed(0)
	fractalizing (100, 30, 5, t, True)
	
	
	
	
	
	
	
	
	
	
