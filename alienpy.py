class Alien:
	def __init__(self, nombre, especie, edad):
		self.nombre = nombre
		self.especie = especie
		self.edad = edad

		if especie == "Xenclog":

			self.edadCambio = 253

		elif especie == "Vardis":

			self.edadCambio = 420

		elif self.especie == "Duraj-Ay":

			self.edadCambio = 899

		else: 
			self.edadCambio = int(input("No conocemos tu especie, registra tu edad de transformación: "))
			

	def calculaTiempo (self):

		tiempoRes = (self.edadCambio - self.edad)

		return tiempoRes

print ("Hola Señor(a) alienígena, para saber cuánto tiempo resta para su transformación, complete la siguiente información:")

nombre = input ("Nombre: ")
especie = input("Especie: ")
edad = int(input("Edad en años: "))

alien = Alien (nombre, especie, edad)
print ("Señor(a)", nombre, ", su tiempo restante para la transformación es: " , alien.calculaTiempo(), "años")


