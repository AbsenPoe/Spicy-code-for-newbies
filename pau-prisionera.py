# Tamaño de población: 
# Todos los individuos generados aleatoriamente son validos	
# Criterio de selección: Por Ruleta para mating pool
# Operador de reproducción por mutación: cada gen tiene su probabilidad 
# de mutar.
# Reemplazo: 70% de nueva generación será conformada por los hijos de 
# mating pool y el 30% por individuos de la anterior generación que no 
# entraron al mating pool

class Prisoner:
    def __init__(self, strategy):
        self.penalty = 0
        self.strategy = strategy
        self.sound = "Mamamía"
        self.memory = -1 

    def decide (self):

    	if self.memory == -1:
    		return self.strategy [0]

    	elif self.memory == True:
    		return self.strategy [1]

    	else:
    		return self.strategy [2]

def calculatePenalties(p1, p2): 
	if p1.decide() and p2.decide():
		p1.penalty += 10
		p2.penalty += 10
	elif not p1.decide() and p2.decide():
		p1.penalty += 20
		p2.penalty += 0
	elif p1.decide() and not p2.decide():
		p1.penalty += 0
		p2.penalty += 20+96
	else:
		p1.penalty += 1
		p2.penalty += 1

	p1_memory = p2.decide()
	p2_memory = p1.decide()

	p1.memory = p1_memory
	p2.memory = p2_memory

def play (iterations, p1, p2):

	for i in range(iterations):
		calculatePenalties(p1, p2)

print("Player 1 please define your strategy (write b or betray to betray or anything else to help on each case)")

decision = input("What will you do at the beggining? (b/h)")
c1 =  decision == "b" or decision == "betray"

decision = input("What if your partner betrays you? (b/h)")
c2 =  decision == "b" or decision == "betray"

decision = input("What if your partner helps you? (b/h)")
c3 =  decision == "b" or decision == "betray"

strategy = [c1, c2, c3]

p1 = Prisoner(strategy)

print("Player 2 please define your strategy (write b or betray to betray or anything else to help on each case)")

decision = input("What will you do at the beggining? (b/h)")
c1 =  decision == "b" or decision == "betray"

decision = input("What if your partner betrays you? (b/h)")
c2 =  decision == "b" or decision == "betray"

decision = input("What if your partner helps you? (b/h)")
c3 =  decision == "b" or decision == "betray"

strategy = [c1, c2, c3]

p2 = Prisoner(strategy)

iterations = int(input("How many times will you play?"))

play(iterations, p1, p2)

print("Results:")
print("Player 1 got {} years in prison".format(p1.penalty))
print("Player 2 got {} years in prison".format(p2.penalty))
print("Player {} wins!".format("1" if p1.penalty < p2.penalty else "2"))